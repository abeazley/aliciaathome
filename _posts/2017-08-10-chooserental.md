---
layout: blog
title:  "Choosing the Perfect Rental Apartment for You!"
date:   2017-08-11
---
<div style="text-align:center"><img src="http://i.imgur.com/TbwH9pF.jpg"></div>

<br/><br/>
As a 27 year old in 2017, I don’t have big dreams of owning a home anytime soon. Honestly, there are lots of things that I love about renting, other than not having to go into debt for 30 years. It’s all about finding the perfect place that fits your lifestyle and not settling for something you’re not going to be comfortable in.
<br/><br/>

<h2>Research the Neighbourhood</h2>

The first thing you want to do is research the area you want to be living in. I spent around 2 months looking at apartments and houses for rent online before going to even see a unit. This gave me a good sense of what types of units are available, pricing, and the neighbourhoods they are in. I recently moved to a new city 1,400 km away, so it was very important to me that we were close to friends, shops, and work, while still in a safe neighbourhood. Something that I found helpful was to map which shops I would frequent in each neighbourhood to see how far of a walk it would be to do daily tasks such as getting groceries. I also used Reddit to review opinions on each neighbourhood, to gauge how safe and compatible they would be with my lifestyle.
<br/><br/>

<h2>Create Ideal Apartment Checklist</h2>

After I chose which neighbourhoods I liked, I made a checklist of things I would look for in an apartment. Here was mine:
<br/><br/>

* A house, duplex, or flat (a unit in a large house)
* Cat friendly
* Deck or outside space big enough for our barbeque
* No carpet
* Large living area
* Bright kitchen
* Dishwasher
* Laundry in the suite
* Minimum 2 bedrooms - one to be used as an office
* Storage space
* Parking
* Not managed by a large management company (I prefer to rent from homeowners who care about their property)

I took my list and scanned online for apartments that were in my desired neighbourhoods that met MOST of these criteria. There were a handful that I liked. If you’re in Canada, [Kijiji](https://www.kijiji.ca) worked really well for me. I liked that I could favourite places and it would stay on a list in the app for me to refer to later on. If I was unsure if a place met specific criteria from my checklist, it was easy to chat with the apartment owners to discuss the features through the app.
<br/><br/>

<h2>Appointments</h2>

For me, I had to drive 1,400 km (870 miles) to see the apartments. I planned a day that both my boyfriend and I could drive up and we booked all of our appointments for the following day. I booked 1 hour time slots for each place, even though each showing was less than 20 minutes. We used the extra time for travel and to discuss what we liked and didn’t like about each appointment, as it’s difficult to talk frankly about a place in front of the person who is trying to rent it to you.

Keep your checklist handy. Go through the unit and look for everything on your list. Ask if you can take photos (great to review when you are discussing the unit after the appointment), if not, make notes! I also like to do a little quality check of the house as well, here is what I look for:
<br/><br/>

* Plugs - if you have a lot of electronics, you need a lot of plugs
* Light fixtures - usually you don’t replace light fixtures in rentals, so it’s good to take a look at what is currently there
* Cleanliness - If there was a huge mess of the fridge and stove, for example, you’re probably going to end up cleaning that when you move in
* Under the sink - look for bugs and mouse droppings
* Paint - if the paint job is old and sun damaged, or if there are kids rooms (clouds on the ceiling, crayons on the wall), ask if the landlord plans to repaint before you move in. The answer here is ‘yes’.
* Is there anything that is really ugly or outdated that you cannot live with? Maybe there is a way that you can hide it or update it for the time that you will be there.

I suggest taking applications from each place that you see, even if you don’t plan on filling them out. After you leave and have time to discuss and really think about the unit, fill out the applications for the units that are the best fit. Filling out more than one application gives you a better chance to get a place because there might be other applications on each place that you see. I filled out 2 applications from the 6 showings we had.
<br/><br/>

<h2>Decision time!</h2>

Once you’ve applied for apartments and have been approved, it’s time to decide which one you are going to sign the lease for. Im a big fan of laying out all of the information so it’s easy to compare. Like I said, we applied for 2 and were actually approved for both. Here is how those 2 places measured up to our checklist:
<br/><br/>

**Check List** | **Option 1** | **Option 2**
--- | :---: | :---: |
**A house, duplex, or flat (a unit in a large house)** |	Townhouse (1) |	A flat in a larger house (1) |
**Cat friendly** | Yes (1)	| Yes (1) | 
**Deck or outside space big enough for our barbecue & patio set**	| Paved area for bbq + small grassy lawn (1) | 	Small deck - may not fit the bbq? Won’t fit patio set (0.5) |
**No Carpet**	| Stairs have carpet (0) | 2nd bedroom has carpet (0) |
**Large Living Area** | If we switched the living and dining rooms, we would have a long living room like we are used to, and a good size dining area. (1) | 	Living room was large, but there was no space for our dining table (0.5) | 
**Bright Kitchen**	| Yes, a large window and door to outside in kitchen (1)	| Yes, at 1 large window in kitchen plus light from other rooms (1) |
**Dishwasher**	| Yes (1)	| Yes (1) |
**Laundry**	| Yes, in the basement (1)	| Yes, in the kitchen (1)
**Minimum 2 bedrooms**	| 3 bedrooms (1)	| 2 bedrooms (1) |
**Storage space** |	Finished large private basement (1) |	Outside below the deck. Small. (0.5) |
**Parking**	| 1 parking spot (1) | 1 parking spot at an additional fee (0.5) |
**Not managed by a large management company** |	Managed by a small management company. Also had contact with the owner. (0.5)	| Managed directly by owner who really cared for the space and was proud of the upgrades that had been done. (1) |
**Score**	| **10.5 points out of 12**	 | **9 points out of 12**| 

<br/>
As you can see, they both did not meet our needs completely, but Option 1 met more things on our checklist than the other did. There were almost no places that didn’t have carpet somewhere, so that is something that we are going to have to learn to live with (hello Dyson).
<br/><br/>
There were also things that checked needs off on my list, but I didn’t really like the idea of. For example, the laundry in the kitchen. We love having people over, and I don’t know how I feel about my dirty clothes being beside where everyone is hanging out. This is something that I could work with (maybe add a curtain) if Option 2 had a lot more great features, but it did not.
<br/><br/>
And there you have it! These are the steps I took to pick the perfect Rental Apartment. The last time I used this process was 4 years ago, which means that this method really work for me to find a great place to live.